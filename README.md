Carbon Diary - Martin Shaw, Cade Miller, Billy Fenton
   
A project created as an entry to the week-long youth coding competition Young Rewired State 2013
   
For more information about this week-long project: https://twitter.com/Carbon_Diary
   

|   	|   	|   	|
|-------|-------|-------|
| ![](http://martinshaw.co/uploaded_files/project_thumbnails/xgcEdRHsThdQp5ALHJVqMqzUTYKtAcgU6a4bwTpp.png) | ![](https://pbs.twimg.com/media/BRKHyWlCYAAXPcW?format=png&name=240x240) | ![](https://pbs.twimg.com/media/BQ6okEFCcAAq7CR?format=png&name=orig) |
